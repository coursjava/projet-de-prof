package com.philippe.league.jpa.web.api;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.philippe.league.jpa.dao.PlayerDAO;
import com.philippe.league.jpa.entity.Player;

@Path("/player")
public class PlayerResource {

	PlayerDAO dao = PlayerDAO.getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Player createOrUpdate(@QueryParam("id") Long id, @QueryParam("name") String name,
			@QueryParam("firstName") String firstName, @QueryParam("jerseyNumber") int jerseyNumber,
			@QueryParam("status") String status, @QueryParam("salary") Double salary) {

		Player player = new Player(name, firstName, jerseyNumber, null, status, null, salary);

		if (id == null) {
			return player = dao.create(player);
		}

		player.setId(id);

		player = dao.update(player);

		return player;
	}

	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Player getPlayer(@PathParam("id") Long id) {

		Player player = (Player) dao.find(Player.class, id);

		return player;

	}

	@Path("/list/{matchingString}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Player> findPlayersLike(@PathParam("matchingString") String matchingString) {
		matchingString = "%" + matchingString + "%";
		return (List<Player>) dao.findLike(matchingString);
	}

	@Path("/list/all")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Player> findAll() {

		return (List<Player>) dao.findAll();
	}

	@Path("{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public void deletePlayer(@PathParam("id") Long id) {

		dao.delete(id);

	}

}
