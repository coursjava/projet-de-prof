package com.philippe.league.jpa.web.api;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import com.philippe.league.jpa.dao.TeamDAO;
import com.philippe.league.jpa.entity.Player;
import com.philippe.league.jpa.entity.Team;

@Path("/team")
public class TeamResource extends Application {

	TeamDAO dao = TeamDAO.getInstance();

	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Team getTeam(@PathParam("id") Long id) {

		Team team = (Team) dao.find(Team.class, id);
		return team;
	}

	@Path("{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteTeam(@PathParam("id") Long id) {
		dao.delete(id);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Team updateTeam(

			@QueryParam("id") Long id, @DefaultValue("") @QueryParam("name") String name,
			@DefaultValue("") @QueryParam("city") String city,
			@DefaultValue("") @QueryParam("division") String division)

	{

		Team team = new Team(name, division, null, city);

		if (id == null) {
			return team = dao.create(team);
		}

		team.setId(id);

		team = dao.update(team);

		return team;
	}

	@Path("/list/all")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Team> findAll() {

		return (List<Team>) dao.findAll();
	}

	@Path("/list/{matchString}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Team> findPlayresLike(@PathParam("matchString") String matchString) {
		matchString = "%" + matchString + "%";
		return (List<Team>) dao.findLike(matchString);
	}

}
