package com.philippe.league.jpa.web.api;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import com.philippe.league.jpa.dao.LeagueDAO;
import com.philippe.league.jpa.entity.League;
import com.philippe.league.jpa.entity.Player;

@Path("/league")
public class LeagueResource extends Application {

	LeagueDAO dao = LeagueDAO.getInstance();

	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public League getTeam(@PathParam("id") Long id) {

		League league = (League) dao.find(League.class, id);
		return league;
	}

	@Path("{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteTeam(@PathParam("id") Long id) {
		dao.delete(id);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public League updateLeague(

			@QueryParam("id") Long id, @DefaultValue("") @QueryParam("name") String name,
			@DefaultValue("") @QueryParam("sport") String sport) {

		League league = new League(name, sport);

		if (id == null) {
			return league = dao.create(league);
		}

		league.setId(id);

		league = dao.update(league);

		return league;
	}

	@Path("/list/all")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<League> findAll() {

		return (List<League>) dao.findAll();
	}

	@Path("/list/{matchString}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<League> findPlayresLike(@PathParam("matchString") String matchString) {
		matchString = "%" + matchString + "%";
		return (List<League>) dao.findLike(matchString);
	}

}
