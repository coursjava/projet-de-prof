package com.philippe.league.jpa.dao;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class GeneralDAO<T> {

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("league");
	EntityManager em = emf.createEntityManager();
	public EntityTransaction transaction = em.getTransaction();

	protected GeneralDAO() {

	}

	private static GeneralDAO instance;

	public static GeneralDAO getInstance() {
		if (instance != null)
			return instance;

		synchronized (GeneralDAO.class) {
			instance = new GeneralDAO<>();
		}
		return instance;

	}

	public void delete(Long id) {

		transaction.begin();
		String sql = "DELETE FROM Player p where p.id=:id";
		em.createQuery(sql).setParameter("id", id).executeUpdate();
		transaction.commit();

	}

	public T create(T toBeCreated) {
		transaction.begin();
		em.persist(toBeCreated);
		em.flush();
		transaction.commit();
		return toBeCreated;
	}

	public void create(List<T> listToBeCreated) {

		// for(T t: listToBeCreated) {
		// create(t);
		// }

		for (Iterator<T> iterator = listToBeCreated.iterator(); iterator.hasNext();) {
			T t = iterator.next();
			create(t);
		}
	}

	public Object find(Class clazz, Long primaryKey) {
		return em.find(clazz, primaryKey);
	}

	public T update(T toBeUpdated) {

		transaction.begin();
		em.merge(toBeUpdated);
		transaction.commit();
		return toBeUpdated;
	}

	public void test(String jpql) {
		em.createQuery(jpql).executeUpdate();
	}

	// public Collection<T> findLike(String matchString) {
	//
	// String jpql = "";
	//
	// "SELECT p FROM Player p where p.name LIKE :mt OR p.firstName LIKE :mt OR
	// p.status LIKE :mt"
	// + " OR p.jerseyNumber LIKE :mt OR p.salary LIKE :mt";
	// return em.createQuery(jpql).setParameter("mt", matchString).getResultList();
	//
	// }

}
