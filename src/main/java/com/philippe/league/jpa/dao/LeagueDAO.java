package com.philippe.league.jpa.dao;

import java.util.Collection;

import com.philippe.league.jpa.entity.League;

public class LeagueDAO extends GeneralDAO<League> {

	private static LeagueDAO instance = null;

	private LeagueDAO() {

	}

	public static LeagueDAO getInstance() {
		if (instance != null)
			return instance;

		synchronized (LeagueDAO.class) {
			instance = new LeagueDAO();
		}
		return instance;

	}

	public Collection<League> findLike(String matchString) {

		String jpql = "SELECT l FROM League l where l.name LIKE :mt OR l.sport LIKE :mt";
		return em.createQuery(jpql).setParameter("mt", matchString).getResultList();

	}
	
	public Collection<League> findAll() {

		String jpql = "SELECT l FROM League l ";
		return em.createQuery(jpql).getResultList();

	}
}
