package com.philippe.league.jpa.dao;

import java.util.Collection;

import com.philippe.league.jpa.entity.Player;

public class PlayerDAO extends GeneralDAO<Player> {

	private static PlayerDAO instance = null;

	private PlayerDAO() {

	}

	public static PlayerDAO getInstance() {
		if (instance != null)
			return instance;

		synchronized (PlayerDAO.class) {
			instance = new PlayerDAO();
		}
		return instance;

	}

	public Collection<Player> findLike(String matchString) {

		String jpql = "SELECT p FROM Player p where p.name LIKE :mt OR p.firstName LIKE :mt OR	 p.status LIKE :mt"
				+ " OR p.jerseyNumber LIKE :mt OR p.salary LIKE :mt";
		return em.createQuery(jpql).setParameter("mt", matchString).getResultList();

	}

	public Collection<Player> findAll() {

		String jpql = "SELECT p FROM Player p ";
		return em.createQuery(jpql).getResultList();

	}

}
