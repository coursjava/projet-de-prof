package com.philippe.league.jpa;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityTransaction;

import com.philippe.league.jpa.dao.GeneralDAO;
import com.philippe.league.jpa.entity.League;
import com.philippe.league.jpa.entity.Player;
import com.philippe.league.jpa.entity.Team;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {

		// League championsLeague = new League("Champions League", "football");
		// Team psg = new Team("psg", "1ere division", championsLeague);
		// Player kurzawa = new Player("kurzawa", " ", 6, psg);

		GeneralDAO dao = GeneralDAO.getInstance();
		
		//System.out.println(dao.test("SELECT DISTINCT p FROM Player p WHERE p.jerseyNumber LIKE '7%'").toString());
		//System.out.println(dao.test("SELECT DISTINCT p FROM Player p WHERE p.salary BETWEEN 1 AND 2").toString());
		
		EntityTransaction transaction = dao.transaction;
		
		transaction.begin();
		dao.test("DELETE FROM Player p WHERE (p.jerseyNumber)<0 ");
		transaction.commit();

		// League nfl = new League("NAF NAF LEAGUE", "football");
		// League league1 = new League("league1", "soccer");
		// League nba = new League("nba", "basketball");
		//
		// List<Team> teams = new ArrayList<Team>();
		// List<League> leagues = new ArrayList<League>(Arrays.asList(nfl, league1,
		// nba));
		// List<Player> players = new ArrayList<Player>();
		//
		// int nbrEquipeParLeague;
		// int nbrEquipes = 10;
		// nbrEquipeParLeague = nbrEquipes / 3;
		// int debut = 0;
		// int fin = nbrEquipeParLeague;
		// Random random = new Random();
		//
		// for (League league : leagues) {
		//
		// for (int i = debut; i < fin; i++) {
		// Team team = new Team("team" + String.valueOf(i), "1ere division", league,
		// "Oakland" + String.valueOf(i));
		//
		// teams.add(team);
		//
		// for (int j = 0; j < 10; j++) {
		// String status = j % 2 == 0 ? "Actif" : "Inactif";
		// Player player = new Player("player" + String.valueOf(j), "Mickael",
		// random.nextInt(), team, status,
		// new Date(random.ints(4).sum()), random.doubles(6).sum());
		// players.add(player);
		// }
		//
		// }
		// Team team1 = new Team("team sans league", "League1", null, "Oakland");
		// teams.add(team1);
		// Player player1 = new Player("player", "Mickael", random.nextInt(), null,
		// "Actif",
		// new Date(random.ints(4).sum()), random.doubles(6).sum());
		// players.add(player1);
		//
		// debut = fin;
		// fin += nbrEquipeParLeague;
		// }
		//
		// EntityTransaction transaction = dao.transaction;
		// try {
		// transaction.begin();
		//
		// dao.create(leagues);
		// dao.create(teams);
		// dao.create(players);
		// transaction.commit();
		//
		// } catch (Exception e) {
		// // TODO: handle exception
		// transaction.rollback();
		// }

		// dao.create(championsLeague);
		// dao.create(psg);
		// dao.create(kurzawa);

		// Player foundPlayer = (Player) dao.find(Player.class, 2L);
		//
		// foundPlayer.setFirstName("matuidi");
		//
		// try {
		// dao.update(foundPlayer);
		// } catch (NoSuchMethodException | SecurityException | IllegalAccessException |
		// IllegalArgumentException
		// | InvocationTargetException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		// System.out.println(foundPlayer.toString());

		System.out.println("Entities created!");

	}
}
