<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Création d'un joueur</title>
<link type="text/css" rel="stylesheet" href="css/style.css" />
</head>
<body>
	<div>
		<form method="get" action="createPlayer">
			<fieldset>
				<legend>Informations du joueur</legend>
				
				<label for="name">Nom </label> 
				<input type="text" id="name" name="name" value="" size="20" maxlength="20" /> <br />
				 
				<label for="firstName">Prénom </label> 
				<input type="text" id="firstName" name="firstName" value="" size="20" maxlength="20" /> <br /> 
				
				<label for="jerseyNumber"> Dossard </label> 
				<input type="number" id="jerseyNumber" name="jerseyNumber" value="" size="20" maxlength="20" /> <br /> 
				
				<label for="status">Statut </label>
				<input type="text" id="status" name="status" value="" size="20" maxlength="20" /> <br /> 
				
				<label for="salary">Salaire </label>
				<input type="text" id="salary" name="salary" value="" size="20" maxlength="20" /> <br />
				
			</fieldset>
			<input type="submit" value="Valider" /> 
			<input type="reset"  value="Remettre à zéro" /> <br />
		</form>
	</div>
</body>
</html>